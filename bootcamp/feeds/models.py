from __future__ import unicode_literals

import re

from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

import operator

from django.db.models import Count

import bleach
from taggit.managers import TaggableManager

from bootcamp.authentication.models import Profile

MAX_REPORT_LIMIT = 1


@python_2_unicode_compatible
class Feed(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    post = models.TextField(max_length=255)
    parent = models.ForeignKey('Feed', null=True, blank=True)
    base = models.ForeignKey('Feed', null=True, blank=True, related_name="retweet")
    likes = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    is_pinned = models.BooleanField(default=0)
    reports = models.IntegerField(default=0)
    tags = TaggableManager()

    class Meta:
        verbose_name = _('Feed')
        verbose_name_plural = _('Feeds')
        ordering = ('-date',)

    def __str__(self):
        return self.post

    @staticmethod
    def get_passed_users_id(user_page):
        following_ids = user_page.profile.get_followings_id()
        muted_users_id = user_page.profile.get_muted_users_id()
        passed_users = [item for item in following_ids if item not in muted_users_id]
        passed_users.append(user_page.profile.id)
        return passed_users

    @staticmethod
    def get_feeds(from_feed=None, user_page=None, report_limit=MAX_REPORT_LIMIT):

        if user_page is not None:
            passed_users_id = Feed.get_passed_users_id(user_page)
            if from_feed is not None:
                feeds = Feed.objects.filter(parent=None, id__lte=from_feed, user__id__in=passed_users_id)
            else:
                feeds = Feed.objects.filter(parent=None, user__id__in=passed_users_id)
        else:
            if from_feed is not None:
                feeds = Feed.objects.filter(parent=None, id__lte=from_feed)
            else:
                feeds = Feed.objects.filter(parent=None)

        return feeds.filter(reports__lt=report_limit)

    @staticmethod
    def get_feeds_after(feed, user_page=None, report_limit=MAX_REPORT_LIMIT):

        if user_page is not None:
            passed_users_id = Feed.get_passed_users_id(user_page)
            feeds = Feed.objects.filter(parent=None, id__gt=feed, user__id__in=passed_users_id)
        else:
            feeds = Feed.objects.filter(parent=None, id__gt=feed)

        return feeds.filter(reports__lt=report_limit)

    @staticmethod
    def get_counted_tags():
        tag_dict = {}
        query = Feed.objects.filter(reports__lt=MAX_REPORT_LIMIT)
        for obj in query:
            for tag in obj.tags.names():
                if tag != '':
                    if tag not in tag_dict:
                        tag_dict[tag] = 1

                    else:  # pragma: no cover
                        tag_dict[tag] += 1

        sorted_tags = sorted(tag_dict.items(), key=operator.itemgetter(1))
        sorted_tags.reverse()
        number_of_trend_tags = 10
        return sorted_tags[:number_of_trend_tags]

    def get_comments(self):
        if self.is_retweeted():
            return self.base.get_comments()
        return Feed.objects.filter(parent=self).order_by('date')

    def calculate_likes(self):
        from bootcamp.activities.models import Activity
        if self.is_retweeted():
            return self.base.calculate_likes()

        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk).count()
        self.likes = likes
        self.save()
        return self.get_num_of_likes()

    def get_likes(self):
        from bootcamp.activities.models import Activity
        if self.is_retweeted():
            return self.base.get_likes()
        likes = Activity.objects.filter(activity_type=Activity.LIKE,
                                        feed=self.pk)
        return likes

    def get_likers(self):
        if self.is_retweeted():
            return self.base.get_likers()
        likes = self.get_likes()
        likers = []
        for like in likes:
            likers.append(like.user)
        return likers

    def get_reports(self):
        from bootcamp.activities.models import Activity
        if self.is_retweeted():
            return self.base.get_reports()
        reports = Activity.objects.filter(activity_type=Activity.REPORT, feed=self.pk)
        return reports

    def get_reporters(self):
        if self.is_retweeted():
            return self.base.get_reporters()
        reports = self.get_reports()
        reporters = []
        for report in reports:
            reporters.append(report.user)
        return reporters

    def calculate_comments(self):
        if self.is_retweeted():
            return self.base.calculate_comments()
        base_feed = self
        while base_feed.parent is not None:
            base_feed = base_feed.parent
        base_feed.comments = Feed.objects.filter(parent=base_feed).count() + Feed.objects.filter(base=base_feed,
                                                                                                 parent_id__gte=1).count()
        self.save()
        return self.comments

    def comment(self, user, post):
        if self.is_retweeted():
            return self.base.comment(user, post)

        feed_comment = Feed(user=user, post=post, parent=self)
        feed_comment.save()
        self.calculate_comments()
        return feed_comment

    def comment_on_comment(self, user, post):
        base_feed = self.parent
        while base_feed.parent is not None:
            base_feed = base_feed.parent
        feed_comment = Feed(user=user, post=post, parent=self, base=base_feed)
        feed_comment.save()
        self.calculate_comments()
        return feed_comment

    def retweet_feed(self, user):
        feed_retweet = Feed(user=user, post="this is retweet.", base=self.get_effective_feed())
        feed_retweet.save()
        src_id = feed_retweet.base.id
        src_feed = Feed.objects.get(pk=src_id)
        self.add_retweet_notif(src_feed.user, feed_retweet)
        return feed_retweet

    def add_retweet_notif(self, src_user, feed_retweet):
        if self.user.pk != src_user.pk:
            feed_retweet.user.profile.notify_reretweeted(self)
        src_user.profile.notify_retweeted(feed_retweet)

    def is_retweeted(self):
        return self.base is not None and self.parent is None

    def get_post_text(self):
        splited_post = self.post.split("tags:")
        return re.sub('@\[(.*?)]\(.*?\)', "@\\1", splited_post[0])

    def get_post(self):
        if self.is_retweeted():
            return self.base.get_post_text()
        else:
            return self.get_post_text()

    def get_real_post(self):
        if self.is_retweeted():
            return self.base.get_real_post()
        else:
            return self.post

    def get_num_of_likes(self):
        if self.is_retweeted():
            return self.base.get_num_of_likes()
        else:
            return self.likes

    def get_num_of_comments(self):
        if self.is_retweeted():
            return self.base.get_num_of_comments()
        else:
            self.calculate_comments()
            return self.comments

    def get_num_of_retweets(self):
        if self.is_retweeted():
            return self.base.get_num_of_retweets()
        else:
            return Feed.objects.filter(base=self, parent=None).count()

    def get_num_of_reports(self):
        if self.is_retweeted():
            return self.base.get_num_of_reports()
        return self.reports

    def linkfy_post(self):
        return bleach.linkify(escape(self.get_real_post()))

    def linkfy_post_with_mention(self):
        return re.sub('@\[(.*?)]\(.*?\)', "<a href='/\\1/'>@\\1</a>", self.linkfy_post())

    def get_mentions(self):
        usernames = re.findall('@\[(.*?)]\(.*?\)', self.post)
        users = User.objects.filter(username__in=usernames)
        return list(users)

    def get_effective_feed(self):
        if self.is_retweeted():
            return self.base
        return self

    def get_tags(self):
        if self.is_retweeted():
            return self.base.get_tags()
        return self.tags.all

    def pin(self):
        pinned_feeds = Feed.objects.filter(user=self.user, is_pinned=1)
        for feed in pinned_feeds:
            feed.unpin()
        self.is_pinned = 1
        return self.save()

    def unpin(self):
        self.is_pinned = 0
        return self.save()

    def calculate_reports(self):
        from bootcamp.activities.models import Activity
        if self.is_retweeted():
            return self.base.calculate_reports()

        reports = Activity.objects.filter(activity_type=Activity.REPORT, feed=self.pk).count()
        self.reports = reports
        self.save()
        return self.get_num_of_reports()
