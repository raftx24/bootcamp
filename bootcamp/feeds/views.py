import json

from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseForbidden)
from django.shortcuts import get_object_or_404, render
from django.template.context_processors import csrf
from django.template.loader import render_to_string

from bootcamp.activities.models import Activity
from bootcamp.decorators import ajax_required
from bootcamp.feeds.models import Feed

from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from bootcamp.questions.models import Question
from bootcamp.articles.models import Article

FEEDS_NUM_PAGES = 10


@login_required
def feeds(request):
    all_feeds = Feed.get_feeds(None, request.user)
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    feeds = paginator.page(1)
    from_feed = -1

    trends = Feed.get_counted_tags()

    return render(request, 'feeds/feeds.html', {
        'feeds': feeds,
        'from_feed': from_feed,
        'page': 1,
        'trends': trends
    })


def feed(request, pk):
    feed = get_object_or_404(Feed, pk=pk)
    return render(request, 'feeds/feed.html', {'feed': feed})


@login_required
@ajax_required
def load(request):
    from_feed = request.GET.get('from_feed')
    page = request.GET.get('page')
    feed_source = request.GET.get('feed_source')
    all_feeds = Feed.get_feeds(from_feed, request.user)
    if feed_source != 'all':
        all_feeds = all_feeds.filter(user__id=feed_source)
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    try:
        feeds = paginator.page(page)
    except PageNotAnInteger:
        return HttpResponseBadRequest()
    except EmptyPage:
        feeds = []
    html = ''
    csrf_token = (csrf(request)['csrf_token'])
    for feed in feeds:
        html = '{0}{1}'.format(html,
                               render_to_string('feeds/partial_feed.html',
                                                {
                                                    'feed': feed,
                                                    'csrf_token': csrf_token,
                                                    'user': request.user.encode('utf-8').strip,
                                                }))
    return HttpResponse(html)


def _html_feeds(last_feed, user, csrf_token, feed_source='all', tags=''):
    feeds = Feed.get_feeds_after(last_feed)
    if feed_source != 'all':
        feeds = feeds.filter(user__id=feed_source)
    html = ''
    for feed in feeds:
        html = '{0}{1}'.format(html,
                               render_to_string('feeds/partial_feed.html',
                                                {
                                                    'feed': feed,
                                                    'user': user,
                                                    'csrf_token': csrf_token,
                                                    'tags': tags
                                                }))

    return html


@login_required
@ajax_required
def load_new(request):
    last_feed = request.GET.get('last_feed')
    user = request.user
    csrf_token = (csrf(request)['csrf_token'])
    html = _html_feeds(last_feed, user, csrf_token)
    return HttpResponse(html)


@login_required
@ajax_required
def check(request):
    last_feed = request.GET.get('last_feed')
    feed_source = request.GET.get('feed_source')
    feeds = Feed.get_feeds_after(last_feed, request.user)
    if feed_source != 'all':
        feeds = feeds.filter(user__id=feed_source)

    count = feeds.count()
    return HttpResponse(count)


@login_required
@ajax_required
def post(request):
    last_feed = request.POST.get('last_feed')
    user = request.user
    csrf_token = (csrf(request)['csrf_token'])
    feed = Feed()
    feed.user = user
    post = request.POST['post']
    post = post.strip()
    tags = request.POST['tags']

    if len(post) > 0:
        feed.post = post[:255]
        feed.save()

    parsed_tags = tags.split(" ")
    for i in xrange(len(parsed_tags)):
        feed.tags.add(parsed_tags[i])

    mentions = feed.get_mentions()
    if mentions:
        for user in mentions:
            user.profile.notify_mentioned(feed)

    html = _html_feeds(last_feed, user, csrf_token, 'all')
    return HttpResponse(html)


@login_required
@ajax_required
def retweet(request):
    feed_id = request.POST['feed']
    feed = Feed.objects.get(pk=feed_id)
    user = request.user
    retweet_feed = feed.retweet_feed(user=user)
    csrf_token = (csrf(request)['csrf_token'])
    html = _html_feeds(retweet_feed.id - 1, user, csrf_token)
    return HttpResponse(html)


@login_required
@ajax_required
def pin(request):
    feed_id = request.POST['feed']
    feed = Feed.objects.get(pk=feed_id)
    if not feed.is_pinned:
        feed.pin()
    else:
        feed.unpin()

    return HttpResponse(1)


@login_required
@ajax_required
def like(request):
    feed_id = request.POST['feed']
    feed = Feed.objects.get(pk=feed_id).get_effective_feed()
    feed_par = Feed.objects.get(pk=feed_id)
    feed_id = feed.id

    user = request.user
    like = Activity.objects.filter(activity_type=Activity.LIKE, feed=feed_id,
                                   user=user)
    if like:
        user.profile.unotify_liked(feed)
        user.profile.unotify_liked(feed_par)
        like.delete()

    else:
        like = Activity(activity_type=Activity.LIKE, feed=feed_id, user=user)
        like.save()
        user.profile.notify_liked(feed)
        user.profile.notify_liked(feed_par)

    return HttpResponse(feed.calculate_likes())


@login_required
@ajax_required
def report(request):
    feed_id = request.POST['feed']
    feed = Feed.objects.get(pk=feed_id).get_effective_feed()
    feed_par = Feed.objects.get(pk=feed_id)
    feed_id = feed.id

    user = request.user
    report = Activity.objects.filter(activity_type=Activity.REPORT, feed=feed_id, user=user)
    if report:
        pass
    else:
        report = Activity(activity_type=Activity.REPORT, feed=feed_id, user=user)
        report.save()

    dummy = feed.calculate_reports()
    return HttpResponse()


@login_required
@ajax_required
def comment(request):
    if request.method == 'POST':
        feed_id = request.POST['feed']
        feed = Feed.objects.get(pk=feed_id).get_effective_feed()
        feed_par = Feed.objects.get(pk=feed_id)
        post = request.POST['post']
        post = post.strip()
        if len(post) > 0:
            post = post[:255]
            user = request.user
            feed_comment = feed.comment(user=user, post=post)
            user.profile.notify_commented(feed)
            user.profile.notify_commented(feed_par)
            user.profile.notify_also_commented(feed)
            mentions = feed_comment.get_mentions()
            print(mentions)
            if mentions:
                for user in mentions:
                    user.profile.notify_mentioned_comment(feed_comment)
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': feed})

    else:
        feed_id = request.GET.get('feed')
        feed = Feed.objects.get(pk=feed_id).get_effective_feed()
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': feed})


@login_required
@ajax_required
def comment_on_comment(request):
    if request.method == 'POST':
        comment_id = request.POST['comment-id']
        comment_feed = Feed.objects.get(pk=comment_id)
        post = request.POST['post']
        post = post.strip()
        if len(post) > 0:
            post = post[:255]
            user = request.user
            comment_feed.comment_on_comment(user=user, post=post)
            user.profile.notify_commented(comment_feed)
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': comment_feed})

    else:
        comment_id = request.GET.get('comment')
        comment = Feed.objects.get(pk=comment_id)
        return render(request, 'feeds/partial_feed_comments.html',
                      {'feed': comment})


@login_required
@ajax_required
def update(request):
    first_feed = request.GET.get('first_feed')
    last_feed = request.GET.get('last_feed')
    feed_source = request.GET.get('feed_source')
    feeds = Feed.get_feeds(None, request.user).filter(id__range=(last_feed, first_feed))
    if feed_source != 'all':
        feeds = feeds.filter(user__id=feed_source)
    dump = {}
    for feed in feeds:
        dump[feed.pk] = {'likes': feed.get_num_of_likes(), 'retweets': feed.get_num_of_retweets(),
                         'comments': feed.get_num_of_comments()}
    data = json.dumps(dump)
    return HttpResponse(data, content_type='application/json')


@login_required
@ajax_required
def track_comments(request):
    feed_id = request.GET.get('feed')
    feed = Feed.objects.get(pk=feed_id)
    if len(feed.get_comments()) > 0:
        return render(
            request, 'feeds/partial_feed_comments.html', {'feed': feed})
    else:
        return HttpResponse()


@login_required
@ajax_required
def remove(request):
    try:
        feed_id = request.POST.get('feed')
        feed = Feed.objects.get(pk=feed_id)
        if feed.user == request.user:
            likes = feed.get_likes()
            parent = feed.parent
            for like in likes:
                like.delete()
            feed.delete()
            if parent:
                parent.calculate_comments()
            return HttpResponse()
        else:
            return HttpResponseForbidden()
    except Exception:
        return HttpResponseBadRequest()
