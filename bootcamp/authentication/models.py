from __future__ import unicode_literals

import hashlib
import os.path
import urllib

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.utils.encoding import python_2_unicode_compatible

from bootcamp.activities.models import Notification


@python_2_unicode_compatible
class Profile(models.Model):
    birthday = models.DateField(max_length=50, null=True, blank=True)
    job_title = models.CharField(max_length=50, null=True, blank=True)
    location = models.CharField(max_length=50, null=True, blank=True)
    muted_users = models.CharField(max_length=500, null=True, blank=True)
    url = models.CharField(max_length=50, null=True, blank=True)
    user = models.OneToOneField(User)
    followings = models.ManyToManyField("self", blank=True, symmetrical=False)
    follow_requests = models.ManyToManyField("self", blank=True, symmetrical=False, related_name="requests")
    celeb_tick = models.BooleanField(default=False)
    private = models.BooleanField(default=False)

    class Meta:
        db_table = 'auth_profile'

    def __str__(self):
        return self.user.username

    def get_follow_requests(self):
        result = self.follow_requests.all()
        return result

    def get_followers(self):
        result = Profile.objects.filter(followings=self.id)
        return result

    def get_followings(self):
        result = self.followings.all()
        return result

    def get_followings_id(self):
        result = [x.id for x in self.followings.all()]
        return result

    def get_muted_users_id(self):
        result = []
        if self.muted_users:
            rejected_names = self.muted_users.split(",")
            for name in rejected_names:
                this_user = User.objects.filter(username=name)
                if this_user:
                    result.append(this_user[0].id)
        return result

    def get_url(self):
        url = self.url
        if "http://" not in self.url and "https://" not in self.url and len(self.url) > 0:  # noqa: E501
            url = "http://" + str(self.url)

        return url

    def get_picture(self):
        no_picture = 'http://trybootcamp.vitorfs.com/static/img/user.png'
        try:
            filename = settings.MEDIA_ROOT + '/profile_pictures/' + \
                       self.user.username + '.jpg'
            picture_url = settings.MEDIA_URL + 'profile_pictures/' + \
                          self.user.username + '.jpg'
            if os.path.isfile(filename):  # pragma: no cover
                return picture_url
            else:  # pragma: no cover
                gravatar_url = 'http://www.gravatar.com/avatar/{0}?{1}'.format(
                    hashlib.md5(self.user.email.lower()).hexdigest(),
                    urllib.urlencode({'d': no_picture, 's': '256'})
                )
                return gravatar_url

        except Exception:
            return no_picture

    def get_screen_name(self):
        try:
            if self.user.get_full_name():
                return self.user.get_full_name()
            else:
                return self.user.username
        except:  # pragma: no cover
            return self.user.username

    def notify_liked(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.LIKED,
                         from_user=self.user, to_user=feed.user,
                         feed=feed).save()

    def unotify_liked(self, feed):
        if self.user != feed.user:
            Notification.objects.filter(notification_type=Notification.LIKED,
                                        from_user=self.user, to_user=feed.user,
                                        feed=feed).delete()

    def notify_retweeted(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.RETWEETED,
                         from_user=feed.user, to_user=self.user,
                         feed=feed).save()

    def unotify_retweeted(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.RETWEETED,
                         from_user=feed.user, to_user=self.user,
                         feed=feed).delete()

    def notify_reretweeted(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.RERETWEETED,
                         from_user=self.user, to_user=feed.user,
                         feed=feed).save()

    def unotify_reretweeted(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.RERETWEETED,
                         from_user=feed.user, to_user=self.user,
                         feed=feed).delete()

    def notify_commented(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.COMMENTED,
                         from_user=self.user, to_user=feed.user,
                         feed=feed).save()

    def notify_also_commented(self, feed):
        comments = feed.get_comments()
        users = []
        for comment in comments:
            if comment.user != self.user and comment.user != feed.user:
                users.append(comment.user.pk)

        users = list(set(users))
        for user in users:
            Notification(notification_type=Notification.ALSO_COMMENTED,
                         from_user=self.user,
                         to_user=User(id=user), feed=feed).save()

    def notify_favorited(self, question):
        if self.user != question.user:
            Notification(notification_type=Notification.FAVORED,
                         from_user=self.user, to_user=question.user,
                         question=question).save()

    def unotify_favorited(self, question):
        if self.user != question.user:
            Notification.objects.filter(
                notification_type=Notification.FAVORED,
                from_user=self.user,
                to_user=question.user,
                question=question).delete()

    def notify_answered(self, question):
        if self.user != question.user:
            Notification(notification_type=Notification.ANSWERED,
                         from_user=self.user,
                         to_user=question.user,
                         question=question).save()

    def notify_accepted(self, answer):
        if self.user != answer.user:
            Notification(notification_type=Notification.ACCEPTED_ANSWER,
                         from_user=self.user,
                         to_user=answer.user,
                         answer=answer).save()

    def notify_mentioned(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.MENTIONED,
                         from_user=feed.user, to_user=self.user,
                         feed=feed).save()

    def notify_mentioned_comment(self, feed):
        if self.user != feed.user:
            Notification(notification_type=Notification.MENTIONED_COMMENT,
                         from_user=feed.user, to_user=self.user,
                         feed=feed).save()

    def unotify_accepted(self, answer):
        if self.user != answer.user:
            Notification.objects.filter(
                notification_type=Notification.ACCEPTED_ANSWER,
                from_user=self.user,
                to_user=answer.user,
                answer=answer).delete()

    def notify_follow_requested(self, sender_profile):
        Notification(notification_type=Notification.FOLLOW_REQUEST,
                     from_user=sender_profile.user,
                     to_user=self.user).save()

    def unotify_follow_requested(self, sender_profile):
        Notification.objects.filter(
                notification_type=Notification.FOLLOW_REQUEST,
                from_user=sender_profile.user,
                to_user=self.user).delete()


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


post_save.connect(create_user_profile, sender=User)
post_save.connect(save_user_profile, sender=User)
