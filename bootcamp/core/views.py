import os
import json

from PIL import Image

from django.conf import settings as django_settings
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render

from bootcamp.core.forms import ChangePasswordForm, ProfileForm
from bootcamp.decorators import ajax_required
from bootcamp.feeds.views import FEEDS_NUM_PAGES, feeds
from bootcamp.feeds.models import Feed
from bootcamp.authentication.models import Profile
from bootcamp.articles.models import Article, ArticleComment
from bootcamp.questions.models import Question, Answer
from bootcamp.activities.models import Activity
from bootcamp.messenger.models import Message


def home(request):
    if request.user.is_authenticated():
        return feeds(request)
    else:
        return render(request, 'core/cover.html')


@login_required
def network(request):
    users_list = User.objects.filter(is_active=True).order_by('username')
    paginator = Paginator(users_list, 100)
    page = request.GET.get('page')
    try:
        users = paginator.page(page)

    except PageNotAnInteger:
        users = paginator.page(1)

    except EmptyPage:  # pragma: no cover
        users = paginator.page(paginator.num_pages)

    return render(request, 'core/network.html', {'users': users})


@login_required
def profile(request, username):
    page_user = get_object_or_404(User, username=username)
    all_feeds = Feed.get_feeds().filter(user=page_user).order_by('-is_pinned')
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    feeds = paginator.page(1)
    likes = Activity.objects.filter(activity_type=Activity.LIKE, user=page_user)

    followings = page_user.profile.followings.all()
    followings_count = page_user.profile.followings.count()
    followers = page_user.profile.get_followers()
    followers_count = page_user.profile.get_followers().count()

    follow_requests = page_user.profile.get_follow_requests()

    from_feed = -1
    if feeds:  # pragma: no cover
        from_feed = feeds[0].id
    likes_count = Activity.objects.filter(activity_type=Activity.LIKE, user=page_user).count()
    feeds_count = Feed.objects.filter(user=page_user).count()
    article_count = Article.objects.filter(create_user=page_user).count()
    article_comment_count = ArticleComment.objects.filter(
        user=page_user).count()
    question_count = Question.objects.filter(user=page_user).count()
    answer_count = Answer.objects.filter(user=page_user).count()
    activity_count = Activity.objects.filter(user=page_user).count()
    messages_count = Message.objects.filter(
        Q(from_user=page_user) | Q(user=page_user)).count()
    data, datepoints = Activity.daily_activity(page_user)
    data = {
        # 'following_list' : following_list,,
        'page_user': page_user,
        'feeds_count': feeds_count,
        'likes_count': likes_count,
        'followings': followings,
        'followings_count': followings_count,
        'followers': followers,
        'follow_requests': follow_requests,
        'followers_count': followers_count,
        'article_count': article_count,
        'article_comment_count': article_comment_count,
        'question_count': question_count,
        'global_interactions': activity_count + article_comment_count + answer_count + messages_count,  # noqa: E501
        'answer_count': answer_count,
        'bar_data': [
            feeds_count, article_count, article_comment_count, question_count,
            answer_count, activity_count],
        'bar_labels': json.dumps('["Feeds", "Articles", "Comments", "Questions", "Answers", "Activities"]'),
        # noqa: E501
        'line_labels': datepoints,
        'line_data': data,
        'feeds': feeds,
        'likes': likes,
        'from_feed': from_feed,
        'page': 1
    }
    return render(request, 'core/profile.html', data)


@login_required
def settings(request):
    user = request.user
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')
            user.profile.job_title = form.cleaned_data.get('job_title')
            user.profile.birthday = form.cleaned_data.get('birthday')
            user.profile.muted_users = form.cleaned_data.get('muted_users')
            user.profile.private = form.cleaned_data.get('private')
            user.email = form.cleaned_data.get('email')
            user.profile.url = form.cleaned_data.get('url')
            user.profile.location = form.cleaned_data.get('location')
            user.save()
            messages.add_message(request,
                                 messages.SUCCESS,
                                 'Your profile was successfully edited.')

    else:
        form = ProfileForm(instance=user, initial={
            'job_title': user.profile.job_title,
            'url': user.profile.url,
            'location': user.profile.location,
            'birthday': user.profile.birthday,
            'muted_users': user.profile.muted_users,
            'private': user.profile.private
        })

    return render(request, 'core/settings.html', {'form': form})


@login_required
def picture(request):
    uploaded_picture = False
    try:
        if request.GET.get('upload_picture') == 'uploaded':
            uploaded_picture = True

    except Exception:  # pragma: no cover
        pass

    return render(request, 'core/picture.html',
                  {'uploaded_picture': uploaded_picture})


@login_required
def password(request):
    user = request.user
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            new_password = form.cleaned_data.get('new_password')
            user.set_password(new_password)
            user.save()
            update_session_auth_hash(request, user)
            messages.add_message(request, messages.SUCCESS,
                                 'Your password was successfully changed.')
            return redirect('password')

    else:
        form = ChangePasswordForm(instance=user)

    return render(request, 'core/password.html', {'form': form})


@login_required
def upload_picture(request):
    try:
        profile_pictures = django_settings.MEDIA_ROOT + '/profile_pictures/'
        if not os.path.exists(profile_pictures):
            os.makedirs(profile_pictures)
        f = request.FILES['picture']
        filename = profile_pictures + request.user.username + '_tmp.jpg'
        with open(filename, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        im = Image.open(filename)
        width, height = im.size
        if width > 350:
            new_width = 350
            new_height = (height * 350) / width
            new_size = new_width, new_height
            im.thumbnail(new_size, Image.ANTIALIAS)
            im.save(filename)

        return redirect('/settings/picture/?upload_picture=uploaded')

    except Exception as e:
        return redirect('/settings/picture/')


@login_required
def save_uploaded_picture(request):
    try:
        x = int(request.POST.get('x'))
        y = int(request.POST.get('y'))
        w = int(request.POST.get('w'))
        h = int(request.POST.get('h'))
        tmp_filename = django_settings.MEDIA_ROOT + '/profile_pictures/' + \
                       request.user.username + '_tmp.jpg'
        filename = django_settings.MEDIA_ROOT + '/profile_pictures/' + \
                   request.user.username + '.jpg'
        im = Image.open(tmp_filename)
        cropped_im = im.crop((x, y, w + x, h + y))
        cropped_im.thumbnail((200, 200), Image.ANTIALIAS)
        cropped_im.save(filename)
        os.remove(tmp_filename)

    except Exception:
        pass

    return redirect('/settings/picture/')


@login_required
@ajax_required
def unfollow(request):
    will_be_followed_id = request.POST['profile_id']
    will_be_followed_profile = Profile.objects.get(pk=will_be_followed_id)

    follower_id = request.user.profile.id
    follower_profile = request.user.profile

    follow_record = Profile.objects.filter(pk=follower_id, followings=will_be_followed_id)

    if follow_record:
        follower_profile.followings.remove(will_be_followed_profile)

    return HttpResponse(0)

@login_required
@ajax_required
def follow_response(request):
    response_value = request.POST['response_value']
    request_sender_id = request.POST['sender_id']

    request_sender_profile = Profile.objects.get(pk=request_sender_id)

    responder_id = request.user.profile.id
    responder_profile = request.user.profile

    follow_request_record = Profile.objects.filter(pk=responder_id, follow_requests=request_sender_id)

    if follow_request_record:
        responder_profile.follow_requests.remove(request_sender_profile)
        if response_value:
            request_sender_profile.followings.add(responder_profile)

    return HttpResponse(0)

@login_required
@ajax_required
def follow_request(request):
    receiver_id = request.POST['profile_id']
    receiver_profile = Profile.objects.get(pk=receiver_id)

    sender_id = request.user.profile.id
    sender_profile = request.user.profile

    follow_request_record = Profile.objects.filter(pk=receiver_id, follow_requests=sender_id)

    if follow_request_record:
        receiver_profile.follow_requests.remove(sender_profile)
        receiver_profile.unotify_follow_requested(sender_profile)


    else:
        receiver_profile.follow_requests.add(sender_profile)
        receiver_profile.notify_follow_requested(sender_profile)

    return HttpResponse(0)


