 $(function () {
   $("div.profile").on("click", ".follow", function () {
        var profile = $(this).closest(".profile");
        var profile_id = $(profile).attr("user-id");
        var csrf = $(profile).attr("csrf");
        var request = $(this).closest(".request");
        $.ajax({
            url: '/follow_request/',
            data: {
                'profile_id': profile_id,
                'csrfmiddlewaretoken': csrf
            },
            type: 'post',
            cache: false,
            success: function () {
                $(".request", profile).removeClass("follow");
                $(".request", profile).addClass("pending");
                    $(".request .text", profile).text("Pending");
                    $(".glyphicon", request).removeClass("glyphicon-plus");
                    $(".glyphicon", request).addClass("glyphicon-adjust");
            }
        });
        return false;
    });

   $("div.profile").on("click", ".unfollow", function () {
        var profile = $(this).closest(".profile");
        var profile_id = $(profile).attr("user-id");
        var csrf = $(profile).attr("csrf");
        var request = $(this).closest(".request");
        $.ajax({
            url: '/unfollow/',
            data: {
                'profile_id': profile_id,
                'csrfmiddlewaretoken': csrf
            },
            type: 'post',
            cache: false,
            success: function () {
                $(".request", profile).addClass("follow");
                $(".request", profile).removeClass("unfollow");
                $(".request .text", profile).text("Follow");
                $(".glyphicon", request).removeClass("glyphicon-minus");
                $(".glyphicon", request).addClass("glyphicon-plus");
            }
        });
        return false;
    });

      $("div.profile").on("click", ".pending", function () {
      console.log("pending");
        var profile = $(this).closest(".profile");
        var profile_id = $(profile).attr("user-id");
        var csrf = $(profile).attr("csrf");
        var request = $(this).closest(".request");

        $.ajax({
            url: '/follow_request/',
            data: {
                'profile_id': profile_id,
                'csrfmiddlewaretoken': csrf
            },
            type: 'post',
            cache: false,
            success: function () {
                $(".request", profile).addClass("follow");
                $(".request", profile).removeClass("pending");
                $(".request .text", profile).text("Follow");
                $(".glyphicon", request).removeClass("glyphicon-adjust");
                $(".glyphicon", request).addClass("glyphicon-plus");
            }
        });
        return false;
    });

     function follow_response(answer) {
         var profile = $(this).closest(".follow_request_item");
         var profile_id = $(profile).attr("sender-id");
         console.log("sender id:" + profile_id);
         var csrf = $(profile).attr("csrf");
         var response = answer;
         console.log("response:" + response);
         $.ajax({
             url: '/follow_response/',
             data: {
                 'sender_id': profile_id,
                 'csrfmiddlewaretoken': csrf,
                 'response_value': response
             },
             type: 'post',
             cache: false,
             success: function () {
                 profile.hide();
             }
         });
         return false;
     }

     $("div.profile").on("click", ".accept", function () {
         return follow_response.call(this, true);
    });

      $("div.profile").on("click", ".reject", function () {
         return follow_response.call(this, false);
    });
 });

